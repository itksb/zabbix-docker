#!/usr/bin/env bash

set -e

source common.sh

for envf in "${ENV_FILE_NAMES[@]}"
do
    [[ ! -f ${envf} ]] && echo "No file ${envf}. Creating from ${envf}.example" && cp -n ${envf}.example ${envf}
done

docker network create -d bridge nginx_net && echo 'Docker network "nginx_net" is created'
docker-compose -f docker-compose.yml up -d

docker-compose ps
