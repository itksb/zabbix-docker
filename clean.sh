#!/usr/bin/env bash
set -e

source common.sh

[[ -d ${ZBX_ENV_DIR} ]] && echo "Deleting zabbix environment directory '${ZBX_ENV_DIR}'" && rm -rf ${ZBX_ENV_DIR}

for envf in "${ENV_FILE_NAMES[@]}"
do
    [[ -f ${envf} ]] && echo "Deleting file ${envf}" && rm  -f ${envf}
done
