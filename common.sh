#!/usr/bin/env bash

export readonly ENV_FILE_NAMES=(".env_web" ".env_agent" ".env_db_mysql" ".env_srv")
export readonly ZBX_ENV_DIR='zbx_env'
